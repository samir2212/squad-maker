package com.squad.maker;

import java.io.Serializable;
import java.util.List;

public class Player implements Serializable{
	
	private static final long serialVersionUID = 8228025920083837116L;

	private String _id;
	private String firstName;
	private String lastName;
	private List<Skill> skills;
	private int averageSkillRating;

	public String get_id() {
		return _id;
	}

	public void set_id(String id) {
		this._id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public List<Skill> getSkills() {
		return skills;
	}

	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}
	
	public Integer getSkillRating(SkillType skillType) {
		if(this.skills == null) {
			return null;
		}
		for (Skill skill : skills) {
			if(skill.getType().equals(skillType))
				return skill.getRating();
		}
		return null;
	}
	

	public int getAverageSkillRating() {
		return averageSkillRating;
	}

	public void setAverageSkillRating(int averageSkillRating) {
		this.averageSkillRating = averageSkillRating;
	}


	class Skill {
		
		private SkillType type;
		private int rating;
		
		public SkillType getType() {
			return type;
		}
		public void setType(String type) {
			this.type = SkillType.valueOf(type);
		}
		public int getRating() {
			return rating;
		}
		public void setRating(int rating) {
			this.rating = rating;
		}

	}
}
