package com.squad.maker;

import java.util.ArrayList;
import java.util.List;

public class Squad {
	
	private List<Player> players;

	public Squad() {
		this.players = new ArrayList<>();
	}
	
	public List<Player> getPlayers() {
		return players;
	}

	public void setPlayers(List<Player> players) {
		this.players = players;
	}
	

}
