package com.squad.maker;

import com.google.gson.annotations.SerializedName;

public enum SkillType {
	
	@SerializedName(value = "Shooting") SHOOTING,
	@SerializedName(value = "Skating") SKATING,
	@SerializedName(value = "Checking") CHECKING;
}
