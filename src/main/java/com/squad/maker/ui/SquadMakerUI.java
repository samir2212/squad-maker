package com.squad.maker.ui;

import static com.squad.maker.SkillType.CHECKING;
import static com.squad.maker.SkillType.SHOOTING;
import static com.squad.maker.SkillType.SKATING;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import com.squad.maker.Player;
import com.squad.maker.Squad;
import com.squad.maker.grid.renderer.GridRenderer;
import com.squad.maker.service.PlayerService;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SuppressWarnings("serial")
@SpringUI
public class SquadMakerUI extends UI {

	@Autowired
	private PlayerService playerService;

	@Autowired
	private GridRenderer gridRenderer;
	
	final Grid<Player> waitingList;

	final ComboBox<Integer> numberOfSquad;

	private final Button createSquadBtn;

	private final Button resetBtn;

	private final VerticalLayout mainLayout;
	private final HorizontalLayout generatedSquads;

	// Initialize the UI
	public SquadMakerUI() {
		this.createSquadBtn = new Button("Create Squads");
		this.resetBtn = new Button("Reset Squads");
		this.waitingList = new Grid<>();
		this.numberOfSquad = new ComboBox<>();
		this.mainLayout = new VerticalLayout();
		this.generatedSquads = new HorizontalLayout();
		this.generatedSquads.setId("generatedSquads");
	}

	@Override
	protected void init(VaadinRequest request) {

		// build layout
		buildLayout();

		// populate waiting list grid
		List<Player> players = playerService.getPlayers();
		gridRenderer.prepareGrid(players, waitingList, false);
		waitingList.setSizeUndefined();

		// populate squad selection combo box
		int size = players.size();
		List<Integer> numberOfSquadList = new ArrayList<>();
		for (int i = 1; i <= size / 2; i++) {
			numberOfSquadList.add(i);
		}
		numberOfSquad.setItems(numberOfSquadList);

		// add listener for create squad button
		createSquadBtn.addClickListener(e -> handleCreateSquad(e));

		// add listener for reset squad button
		resetBtn.addClickListener(e -> handleResetSquad(e));

	}

	/**
	 * Builds the UI layout and arranges the components.
	 */
	protected void buildLayout() {
		Label createSquadLabel = new Label("Select the number of squads");
		HorizontalLayout filters = new HorizontalLayout(createSquadLabel, numberOfSquad, createSquadBtn, resetBtn);
		Label waitingListLabel = new Label();
		waitingListLabel.setCaption("<b>Waiting List</b>");
		waitingListLabel.setCaptionAsHtml(true);
		mainLayout.addComponents(filters, waitingListLabel, waitingList);
		setContent(mainLayout);
	}

	/**
	 * Handles the click event of reset button. Clears the generated squads and
	 * re-populates waiting list grid.
	 * 
	 * @param e
	 *            click event of reset button
	 */
	protected void handleResetSquad(ClickEvent e) {
		generatedSquads.removeAllComponents();
		mainLayout.removeComponent(generatedSquads);
		numberOfSquad.clear();
		waitingList.setItems(playerService.getPlayers());
		waitingList.setSizeUndefined();
	}

	/**
	 * Handles the click event of create squad button and generates the squads.
	 * 
	 * @param e
	 *            click event of create squad button
	 */
	protected void handleCreateSquad(ClickEvent e) {
		
		if(StringUtils.isEmpty(numberOfSquad.getValue())) {
			Notification.show("Please select number of squads.", Notification.Type.ERROR_MESSAGE);
			return;
		}
		// clear previously generated components if there are any
		generatedSquads.removeAllComponents();
		mainLayout.removeComponent(generatedSquads);
		
		int numberOfSquads = numberOfSquad.getValue();
		List<Player> players = playerService.getPlayers();
		
		calculateAverageSkillRating(players);
		
		List<Squad> squads = generateSquads(players, numberOfSquads);
		
		renderSquads(squads);
	}
	
	/**
	 * This method sorts the waiting list players according to their average skill
	 * rating and adds them to the squads.
	 * 
	 * @param players
	 *            - all registered players list
	 * @param numberOfSquads
	 *            - number of squads to be generated
	 * @return List of squads with balanced skill distribution
	 */
	private List<Squad> generateSquads(List<Player> players, int numberOfSquads) {
		List<Squad> squads = new ArrayList<>();
		int playersInEachSquad = players.size() / numberOfSquads;
		players.sort((p1, p2) -> {
			return p1.getAverageSkillRating() - p2.getAverageSkillRating();
		});
		for (int i = 0; i < numberOfSquads; i++) {
			squads.add(new Squad());
		}
		for (int i = 0; i < playersInEachSquad; i++) {
			if (i % 2 == 0) {
				for (int j = 0; j < numberOfSquads; j++) {
					squads.get(j).getPlayers().add(players.remove(0));
				}
			} else {
				for (int j = numberOfSquads - 1; j >= 0; j--) {
					squads.get(j).getPlayers().add(players.remove(0));
				}

			}
		}
		waitingList.setItems(players);
		waitingList.setHeightByRows(Math.max(1, players.size()));
		return squads;
	}

	/**
	 * Renders the generated squads on UI by creating multiple grids.
	 * 
	 * @param squads
	 */
	protected void renderSquads(List<Squad> squads) {
		// arrange generated squads in two vertical layouts
		VerticalLayout verticalLayoutLeft = new VerticalLayout();
		VerticalLayout verticalLayoutRight = new VerticalLayout();

		for (int i = 0; i < squads.size(); i++) {
			Squad squad = squads.get(i);
			Grid<Player> newSquad = new Grid<>();
			Label squadLabel = new Label();
			squadLabel.setCaption("<b>Squad " + (i + 1) + "</b>");
			squadLabel.setCaptionAsHtml(true);
			newSquad.setItems(squad.getPlayers());
			gridRenderer.prepareGrid(squad.getPlayers(), newSquad, true);
			if (i % 2 == 0) {
				verticalLayoutLeft.addComponents(squadLabel, newSquad);
			} else {
				verticalLayoutRight.addComponents(squadLabel, newSquad);
			}
		}
		generatedSquads.addComponents(verticalLayoutLeft, verticalLayoutRight);
		mainLayout.addComponent(generatedSquads);
	}

	private void calculateAverageSkillRating(List<Player> players) {
		players.forEach(player -> {
			player.setAverageSkillRating(
					(player.getSkillRating(SKATING) + player.getSkillRating(SHOOTING) + player.getSkillRating(CHECKING))
							/ 3);
		});
	}	
}
