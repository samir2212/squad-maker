package com.squad.maker.service;

import java.io.FileReader;
import java.io.IOException;
import java.util.List;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import com.squad.maker.Player;
import com.squad.maker.Squad;

@Component
public class PlayerService {

	private static final String PLAYERS_JSON = "players.json";
	private final Gson gson = new Gson();

	/**
	 * This method returns the players from the JSON file.
	 * 
	 */
	public List<Player> getPlayers() {
		try {
			Resource resource = new ClassPathResource(PLAYERS_JSON);
			JsonReader reader = new JsonReader(new FileReader(resource.getFile()));
			Squad squad = gson.fromJson(reader, Squad.class);
			return squad.getPlayers();
		} catch (IOException e) {
			// log the error
			e.printStackTrace();
		}
		return null;
	}

}
