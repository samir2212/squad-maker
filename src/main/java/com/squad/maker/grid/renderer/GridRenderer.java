package com.squad.maker.grid.renderer;

import static com.squad.maker.SkillType.CHECKING;
import static com.squad.maker.SkillType.SHOOTING;
import static com.squad.maker.SkillType.SKATING;

import java.util.List;

import org.springframework.stereotype.Component;

import com.squad.maker.Player;
import com.vaadin.ui.Grid;
import com.vaadin.ui.components.grid.FooterRow;

@Component
public class GridRenderer {
	
	private static final String NAME_HEADER = "Name";
	private static final String SPACE = " ";
	

	/**
	 * @param players
	 *            - players to be added to the grid
	 * @param playerGrid
	 *            - a grid instance which needs to be populated
	 * @param appendFooter
	 *            - indicates if footer row needs to be added or not
	 */
	public void prepareGrid(List<Player> players, Grid<Player> playerGrid, boolean appendFooter) {
		playerGrid.addColumn(p -> playerNameRenderer(p)).setCaption(NAME_HEADER).setExpandRatio(4).setId(NAME_HEADER);
		playerGrid.addColumn(p -> p.getSkillRating(SKATING)).setCaption(SKATING.toString()).setExpandRatio(2)
				.setId(SKATING.toString());
		playerGrid.addColumn(p -> p.getSkillRating(SHOOTING)).setCaption(SHOOTING.toString()).setExpandRatio(2)
				.setId(SHOOTING.toString());
		playerGrid.addColumn(p -> p.getSkillRating(CHECKING)).setCaption(CHECKING.toString()).setExpandRatio(2)
				.setId(CHECKING.toString());
		playerGrid.setItems(players);

		if (appendFooter)
			appendFooterRow(playerGrid, players);
		playerGrid.setHeightByRows(players.size());
	}

	private void appendFooterRow(Grid<Player> playerGrid, List<Player> players) {
		FooterRow footerRow = playerGrid.appendFooterRow();
		footerRow.getCell(NAME_HEADER).setHtml("<b>Average</b>");
		int sumSkating = 0, sumShooting = 0, sumChecking = 0;
		for (Player player : players) {
			sumSkating += player.getSkillRating(SKATING);
			sumShooting += player.getSkillRating(SHOOTING);
			sumChecking += player.getSkillRating(CHECKING);
		}
		int totalPlayers = players.size();
		footerRow.getCell(SKATING.toString()).setHtml("<b>" + String.valueOf(sumSkating / totalPlayers) + "</b>");
		footerRow.getCell(SHOOTING.toString()).setHtml("<b>" + String.valueOf(sumShooting / totalPlayers) + "</b>");
		footerRow.getCell(CHECKING.toString()).setHtml("<b>" + String.valueOf(sumChecking / totalPlayers) + "</b>");
	}

	private String playerNameRenderer(Player player) {
		StringBuilder sb = new StringBuilder();
		sb.append(player.getFirstName()).append(SPACE).append(player.getLastName());
		return sb.toString();
	}
}
