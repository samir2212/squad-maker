## How to use the application?

To compile the entire project, run "mvn install".

To run the application, run it as a spring boot application and open http://localhost:8080/ .

To produce a deployable production mode WAR:

* run "mvn clean package"
* test the war file with Tomcat server

Application is deployed at: http://squad-maker-env.us-east-2.elasticbeanstalk.com/

## Squad Maker Challenge

This coding assignment has been implemented using Java and Vaadin application framework. Find more details about Vaadin at https://vaadin.com/ .

It is a single page application. The landing page contains following.

* Select number of squads combobox
* Create Squads button
* Reset Squads button
* Players in waiting list

## Workflow

* "Select the number of squads" combobox allows user to select number of squads he/she wants to create. The maximum value of combobox is kept as half of the all players list with an assumption that each squad will have a minimum of two players.
* "Create Squads" button generates the selected squads.
* "Reset Squads" button resets the screen and all the players move to waiting list.

